FROM sbvr/laravel-composer as build
WORKDIR /tmp

COPY ./ /tmp/

WORKDIR /tmp/apps/$APP_NAME
RUN composer self-update --2
RUN COMPOSER_MIRROR_PATH_REPOS=1 composer install

FROM sbvr/laravel-$LARAVEL_DOCKER_TYPE
COPY --from=build /tmp/apps/$APP_NAME /var/www/html/
