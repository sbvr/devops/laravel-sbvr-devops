FROM sbvr/laravel-composer as build
WORKDIR /tmp
COPY composer.json composer.lock auth.json* /tmp/

RUN composer self-update --2
RUN COMPOSER_MIRROR_PATH_REPOS=1 composer install --no-scripts --no-autoloader

COPY ./ /tmp/
RUN composer dump-autoload

FROM sbvr/laravel-$LARAVEL_DOCKER_TYPE
COPY --from=build /tmp /var/www/html/
